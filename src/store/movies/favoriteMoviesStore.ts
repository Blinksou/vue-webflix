import { defineStore } from "pinia";
import { Movie } from "@/types/movies";

export const useFavoriteMoviesStore = defineStore("favoriteMovies", {
  state: () => ({
    favoriteMovies: [] as Array<Movie>,
  }),
  getters: {
    isFavorite: (state) => (movie: Movie) => {
      return state.favoriteMovies.some((movieT) => movieT.id === movie?.id);
    },
  },
  actions: {
    toggleFavorite(movie: Movie) {
      const index = this.favoriteMovies.findIndex(
        (movieT) => movieT.id === movie.id
      );
      if (index > -1) {
        this.favoriteMovies.splice(index, 1);
      } else {
        this.favoriteMovies.push(movie);
      }
    },
  },
  persist: true,
});

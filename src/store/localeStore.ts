import { defineStore } from "pinia";

export const useLocaleStore = defineStore("locale", {
  state: () => ({
    locale: "en" as string,
    availableLocales: ["en", "fr"] as Array<string>,
  }),
  actions: {
    setLocale(locale: string) {
      if (this.availableLocales.includes(locale)) {
        this.locale = locale;
      }
    },
  },
  persist: true,
});

export type Cast = {
  id: number;
  name: string;
  profile_path: string | null;
  gender: number | null;
  adult: boolean;
  original_name: string;
  popularity: number;
  cast_id: number;
  character: string;
  credit_id: string;
  order: number;
  known_for_department: string;
};

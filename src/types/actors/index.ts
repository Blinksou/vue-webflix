import { Cast } from "@/types/cast";
import { Crew } from "@/types/crew";

export type ActorWithMovieCreditsApiResponse = {
  adult: boolean;
  also_known_as: string[];
  biography: string;
  birthday: string;
  deathday: string | null;
  gender: number | null;
  homepage: string;
  id: number;
  imdb_id: string;
  known_for_department: string;
  movie_credits: [Cast[], Crew[]];
  name: string;
  place_of_birth: string;
  popularity: number;
  profile_path: string | null;
};

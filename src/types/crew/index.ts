export type Crew = {
  id: number;
  name: string;
  job: string;
  profile_path: string;
  gender: number | null;
  adult: boolean;
  known_for_department: string;
  popularity: number;
  credit_id: string;
  department: string;
};

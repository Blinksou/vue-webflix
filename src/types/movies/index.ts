import { Genre } from "@/types/genres";
import { Cast } from "@/types/cast";
import { Crew } from "@/types/crew";

export type Movie = {
  id: number;
  adult: boolean;
  backdrop_path: string;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  vote_average: number;
  vote_count: number;
  genre_ids: Array<number>;
  genres?: Array<Genre>;
};

export type MovieDetails = {
  id: number;
  adult: boolean;
  backdrop_path: string;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  vote_average: number;
  vote_count: number;
  genres?: Array<Genre>;
  budget: number;
  revenue: number;
  runtime: number | null;
};

export type MovieApiResponse = {
  page: number;
  total_results: number;
  total_pages: number;
  results: Movie[];
};

export type MovieCreditsApiResponse = {
  id: number;
  cast: Array<Cast>;
  crew: Array<Crew>;
};

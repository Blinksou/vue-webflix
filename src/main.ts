import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { createPinia } from "pinia";
import "./index.css";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";
import { createI18n } from "vue-i18n";
import fr from "./translations/fr.json";
import en from "./translations/en.json";
import { createHead } from "@vueuse/head";

const pinia = createPinia().use(piniaPluginPersistedstate);

const i18n = createI18n({
  locale: "en",
  globalInjection: true,
  messages: {
    fr,
    en,
  },
});

const head = createHead();

createApp(App).use(router).use(pinia).use(i18n).use(head).mount("#app");

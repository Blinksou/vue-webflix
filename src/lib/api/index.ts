import axios, { AxiosInstance } from "axios";

export const api: AxiosInstance = axios.create({
  baseURL: process.env.VUE_APP_API_URL ?? "https://api.themoviedb.org/3",
  params: {
    api_key: process.env.VUE_APP_API_KEY ?? "",
  },
});

import { Movie } from "@/types/movies";

export function getPosterPath(movie: Movie): string {
  return movie?.poster_path
    ? `https://image.tmdb.org/t/p/w200${movie.poster_path}`
    : "http://placehold.jp/200x300.png";
}

export function getBackdropPath(movie: Movie): string {
  return `https://image.tmdb.org/t/p/w600${movie?.backdrop_path}`;
}

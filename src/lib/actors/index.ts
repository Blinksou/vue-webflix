export function getActorImage(profilePath: string): string {
  return profilePath
    ? `https://image.tmdb.org/t/p/w400${profilePath}`
    : "https://placehold.jp/200x300.png";
}
